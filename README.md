# Projekt


## Konfiguracja repozytorium
**Aby pracować w repozytorium należy utworzyć branch z brancha main
o nazwie s_numer_indeksu.**


## Terminy

Szkic projektu - diagram paint / mermaid / cokolwiek

- grupa 1 (niedziela) - 26.11.2023
- grupa 2 (sobota) - 25.11.2023

Checkpoint - sprawdzenie dotychczasowych postępów pracy

- grupa 1 (niedziela) - 17.12.2023
- grupa 2 (sobota) - 16.12.2023

Termin oddania projektu

- grupa 1 (niedziela) - 28.01.2024
- grupa 2 (sobota) - 27.01.2024

